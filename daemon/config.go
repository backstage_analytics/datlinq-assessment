package daemon

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

type Configuration struct {
	Db struct {
		DNS        string `yaml:"dns"`
		Database   string `yaml:"database"`
		Collection string `yaml:"collection"`
	} `yaml:"db"`

	// Path string `yaml:"path"`
}

// ReadConfigFromFile reads config.yaml and pushes to Config
func ReadConfigFromFile(filename string) error {
	// open config.yaml file
	data, err := ioutil.ReadFile(filename)

	if err != nil {
		return err
	}

	// get data from config.yaml file and umarshal it in Config struct
	err = yaml.Unmarshal(data, &Config)

	if err != nil {
		return err
	}

	return nil
}

// Config variable to access Config struct
var Config = Configuration{}
