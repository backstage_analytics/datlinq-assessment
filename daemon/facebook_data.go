package daemon

import "gopkg.in/mgo.v2/bson"

type FacebookObject struct {
	MongoId           bson.ObjectId `json:"_id" bson:"_id"`
	Timestamp         float64       `json:"__timestamp" bson:"__timestamp"`
	Category          string        `json:"category" bson:"category"`
	Id                string        `json:"id" bson:"id"`
	Name              string        `json:"name" bson:"name"`
	About             string        `json:"about" bson:"about"`
	CanPost           bool          `json:"can_post" bson:"can_post"`
	HasAddedApp       bool          `json:"has_added_app" bson:"has_added_app"`
	IsCommunityPage   bool          `json:"is_community_page" bson:"is_community_page"`
	IsVerified        bool          `json:"is_verified" bson:"is_verified"`
	Likes             float64       `json:"likes" bson:"likes"`
	Link              string        `json:"link" bson:"link"`
	TalkingAboutCount float64       `json:"talking_about_count" bson:"talking_about_count"`
	Website           string        `json:"website" bson:"website"`
	WereHereCount     float64       `json:"were_here_count" bson:"were_here_count"`
	Phone             string        `json:"phone" bson:"phone"`
	Description       string        `json:"description" bson:"description"`
	Birthday          string        `json:"birthday" bson:"birthday"`
	Founded           string        `json:"founded" bson:"founded"`
	Mission           string        `json:"mission" bson:"mission"`
	PriceRange        string        `json:"price_range" bson:"price_range"`
	PersonalInfo      string        `json:"personal_info" bson:"personal_info"`
	Awards            string        `json:"awards" bson:"awards"`

	// Location           LocationComponent `json:"location" bson:"location"`
	// CoordinateLocation LocationObject `json:"__location" bson:"__location"`
	// CategoryList       []Category        `json:"category_list" bson:"category_list"`

}

type LocationObject struct {
	Type       string    `json:"type" bson:"type"`
	Coordinate []float64 `json:"coordinates" bson:"coordinates"`
}

type Category struct {
	Id   string `json:"id" bson:"id"`
	Name string `json:"name" bson:"name"`
}

type LocationComponent struct {
	City      string  `json:"city" bson:"city"`
	Country   string  `json:"country" bson:"country"`
	Latitude  float64 `json:"latitude" bson:"latitude"`
	Longitude float64 `json:"longitude" bson:"longitude"`
}
