package daemon

import "gopkg.in/mgo.v2/bson"

type GoogleObject struct {
	MongoId bson.ObjectId `json:"_id" bson:"_id" csv:""`

	AddressURL               string `json:"adr_address" bson:"adr_address" csv:""`
	FormattedAddress         string `json:"formatted_address" bson:"formatted_address" csv:""`
	FormattedPhoneNumber     string `json:"formatted_phone_number" bson:"formatted_phone_number" csv:""`
	Icon                     string `json:"icon" bson:"icon"`
	Id                       string `json:"id" bson:"id"`
	InternationalPhoneNumber string `json:"international_phone_number" bson:"international_phone_number"`
	Name                     string `json:"name" bson:"name"`
	PlaceId                  string `json:"place_id" bson:"place_id"`
	Reference                string `json:"reference" bson:"reference"`
	Scope                    string `json:"scope" bson:"scope"`
	Url                      string `json:"url" bson:"url"`
	UTC_offset               string `json:"utc_offset" bson:"utc_offset"`
	Vicinity                 string `json:"vicinity" bson:"vicinity"`
}

// Loc                      LocComponent `json:"loc" bson:"loc"`
// Types                    []string `json:"types" bson:"types"`

// Geometry             struct {
// 	Location struct {
// 		LatLngComponent
// 	} `json:"location" bson:"location"`
// 	Viewpoint struct {
// 		Northeast struct {
// 			LatLngComponent
// 		} `json:"northeast" bson:"northeast"`
// 		Northwest struct {
// 			LatLngComponent
// 		} `json:"northwest" bson:"northwest"`
// 		Southeast struct {
// 			LatLngComponent
// 		} `json:"southeast" bson:"southeast"`
// 		Southwest struct {
// 			LatLngComponent
// 		} `json:"southwest" bson:"southwest"`
// 	}
// } `json:"geometry" bson:"geometry"`

// AddressComponents    []AddressComponent `json:"address_components" bson:"address_components"` // Array of address components
// Support component for Google Objects
type AddressComponent struct {
	LongName  string   `json:"long_name" bson:"long_name" csv:"long_name"`
	ShortName string   `json:"short_name" bson:"short_name" csv:"short_name"`
	Types     []string `json:"types" bson:"types" csv:"types"`
}

type LatLngComponent struct {
	Lat float64 `json:"lat" bson:"lat" csv:"lat"`
	Lng float64 `json:"lng" bson:"lng" csv:"lng"`
}

type LocComponent struct {
	Type        string    `json:"type" bson:"type" csv:"type"`
	Coordinates []float64 `json:"coordinates" bson:"coordinates" csv:"coordinates"`
}
