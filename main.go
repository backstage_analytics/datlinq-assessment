package main

import (
	"encoding/csv"
	"flag"
	"io"
	"log"
	"os"

	"github.com/gocarina/gocsv"

	"bitbucket.org/backstage-analytics/datlinq-assessment/daemon"

	"gopkg.in/mgo.v2"
)

// initiate mongo session only once
var Session *mgo.Session

var configFile = flag.String("c", "config.yaml", "Config file location")

func main() {

	log.Println("Starting ingestion of " + daemon.Config.Db.Database + " data...")

	// dial-up connectino to MongoDB based on config.go connection
	Session, err := mgo.Dial(daemon.Config.Db.DNS)
	if err != nil {
		log.Fatal("Unable to connect to mongo: " + err.Error())
	}

	defer Session.Close()
	// Optional. Switch the session to a monotonic behavior
	Session.SetMode(mgo.Monotonic, true)

	// determine which object model to use based on config options: google or facebook
	switch daemon.Config.Db.Database {

	case "google":

		// create cursor for 'google' database and 'sample' collection
		c := Session.DB(daemon.Config.Db.Database).C(daemon.Config.Db.Collection)

		var GoogleData []daemon.GoogleObject

		// Query all mongo documents
		err = c.Find(nil).All(&GoogleData)
		if err != nil {
			log.Fatal("Could not Retrieve all docs: ", err.Error())
		}

		log.Println("Finished ingestion of data from MongoDb")

		// write all mongo documents into gl_data.csv file (google data)
		file, err := os.Create("gl_data.csv")
		if err != nil {
			log.Fatal("Could not create file with that name:", err.Error())
		}

		// defer closing file after finishing up writing to it
		defer file.Close()

		log.Println("Started to write to gl_data.csv file")

		// serializer for custom golang structs to .CSV
		gocsv.SetCSVWriter(func(out io.Writer) *csv.Writer {
			return csv.NewWriter(out)
		})

		gocsv.MarshalFile(&GoogleData, file)

		log.Println("Finished writing data to gl_data.csv file")

	case "facebook":

		c := Session.DB(daemon.Config.Db.Database).C(daemon.Config.Db.Collection)

		var FacebookData []daemon.FacebookObject

		// Query all mongo documents
		err = c.Find(nil).All(&FacebookData)
		if err != nil {
			log.Fatal("Could not Retrieve all docs: ", err.Error())
		}

		log.Println("Finished ingestion of data from MongoDb")

		// write all mongo documents into fb_data.csv file (facebook data)
		file, err := os.Create("fb_data.csv")
		if err != nil {
			log.Fatal("Could not create file with that name: ", err.Error())
		}

		// serializer for custom golang structs to .CSV
		defer file.Close()

		log.Println("Started to write to fb_data.csv file")

		gocsv.SetCSVWriter(func(out io.Writer) *csv.Writer {
			return csv.NewWriter(out)
		})

		gocsv.MarshalFile(&FacebookData, file)

		log.Println("Finished writing data to fb_data.csv file")

	default:
		log.Println("Currently only supported databases in mongo are 'google' and 'facebook'. User input is: " + daemon.Config.Db.Database)
	}

}

func init() {

	flag.Parse()

	err := daemon.ReadConfigFromFile(*configFile)

	if err != nil {
		log.Fatal("Could not read config file:", err)
	}

}
